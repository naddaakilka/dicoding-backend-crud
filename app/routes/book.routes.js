module.exports = (app) => {
  const books = require('../controllers/book.controller.js');

  // Create
  app.post('/create', books.create);

  // GetAll
  app.get('/getall', books.findAll);

  // Get By Id
  app.get('/getbyid/:id', books.findOne);

  // Update
  app.put('/update/:id', books.update);

  // Delete
  app.delete('/delete/:id', books.delete);
};

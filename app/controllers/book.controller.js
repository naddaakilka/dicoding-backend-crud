const Book = require('../models/book.model.js');

// Create and Save a new Book
exports.create = (req, res) => {
  // Validate request
  if (!req.body.genre) {
    return res.status(400).send({
      message: 'Form Cannot be Empty',
    });
  }

  // Create a Book
  const book = new Book({
    title: req.body.title || 'Untitled Book',
    genre: req.body.genre,
    synopsis: req.body.synopsis,
    isbn: req.body.isbn,
  });

  // Save Book in the database
  book
    .save()
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Some error occurred while creating the Book.',
      });
    });
};

// Retrieve and return all books from the database.
exports.findAll = (req, res) => {
  Book.find()
    .then((books) => {
      res.send(books);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving data.',
      });
    });
};

// Find a single book with a id
exports.findOne = (req, res) => {
  Book.findById(req.params.id)
    .then((book) => {
      if (!book) {
        return res.status(404).send({
          message: 'Book Not Found' + req.params.id,
        });
      }
      res.send(book);
    })
    .catch((err) => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          message: 'Book Not Found' + req.params.id,
        });
      }
      return res.status(500).send({
        message: 'Error retrieving book with id ' + req.params.id,
      });
    });
};

// Update a book identified by the id in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body.genre) {
    return res.status(400).send({
      message: 'Form Cannot be Empty',
    });
  }

  // Find book and update it with the request body
  Book.findByIdAndUpdate(
    req.params.id,
    {
      title: req.body.title || 'Untitled Book',
      genre: req.body.genre,
      synopsis: req.body.synopsis,
      isbn: req.body.isbn,
    },
    { new: true },
  )
    .then((book) => {
      if (!book) {
        return res.status(404).send({
          message: 'Book Not Found' + req.params.id,
        });
      }
      res.send(book);
    })
    .catch((err) => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          message: 'Book Not Found' + req.params.id,
        });
      }
      return res.status(500).send({
        message: 'Update Failed' + req.params.id,
      });
    });
};

// Delete a book with the specified id in the request
exports.delete = (req, res) => {
  Book.findByIdAndRemove(req.params.id)
    .then((book) => {
      if (!book) {
        return res.status(404).send({
          message: 'Book Not Found' + req.params.id,
        });
      }
      res.send({ message: 'Deleted Successfully!' });
    })
    .catch((err) => {
      if (err.kind === 'ObjectId' || err.name === 'NotFound') {
        return res.status(404).send({
          message: 'Book Not Found' + req.params.id,
        });
      }
      return res.status(500).send({
        message: 'Could Not Delete' + req.params.id,
      });
    });
};

const mongoose = require('mongoose');

const BookSchema = mongoose.Schema(
  {
    title: String,
    genre: String,
    synopsis: String,
    isbn: Number,
  },
  {
    timestamps: true,
  },
);

module.exports = mongoose.model('Book', BookSchema);
